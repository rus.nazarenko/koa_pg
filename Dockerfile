FROM node:14.17.3		

WORKDIR /server

COPY package*.json ./

RUN npm install
# RUN npm install -g nodemon

COPY . . 		

ENV TZ Europe/Kiev

CMD npm run start