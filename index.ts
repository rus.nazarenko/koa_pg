import * as Koa from 'koa'
const app = new Koa()
import * as bodyParser from 'koa-bodyparser'
import userRouter from './src/components/users/userRouter'
import * as logger from 'koa-logger'
const APP_PORT = process.env.APP_PORT

app.use(logger())
app.use(bodyParser())
app.use(userRouter.routes())


app.listen(APP_PORT, () => {
  console.log(`server is listening on port ${APP_PORT}`)
})