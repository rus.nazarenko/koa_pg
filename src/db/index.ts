import { Pool } from 'pg'
import dbConfig from '../config/db'

const pool = new Pool(dbConfig)

export default pool