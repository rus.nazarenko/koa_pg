import pool from '../index'

export const createTable = async () => {
  try {
    const query1 = `CREATE TABLE IF NOT EXISTS users (
      id serial, 
      user_name VARCHAR(20) NOT NULL,
      role VARCHAR(20) NOT NULL,
      password VARCHAR(20) NOT NULL,
      email VARCHAR(50) UNIQUE  NOT NULL
      )`
    await pool.query(query1)
    console.log("Migrations Ok")
  } catch (err) {
    console.log("Migrations error", err)
  }
}


// const { Pool } = require('pg')
// const dbConfig = require('../../config/db')

// console.log("dbConfig", dbConfig)

// const pool = new Pool(dbConfig)

// console.log("CREATE TABLE")

// const query1 = `CREATE TABLE IF NOT EXISTS users (
//       id serial, 
//       user_name VARCHAR(20) NOT NULL,
//       role VARCHAR(20) NOT NULL,
//       password VARCHAR(20) NOT NULL,
//       email VARCHAR(50) UNIQUE  NOT NULL
//       )`
// pool.query(query1)
//   .then((res) => { console.log("Migrations Ok", res) 
//   pool.end()})
//   .catch((err) => { console.log("Migrations error", err) })