import {Context} from 'koa'
import {addUserQuery, delUserQuery, updateUserQuery, getUsersQuery} from './userService'

export const addUserController = async (ctx: Context) => {
  try {
    if (Object.entries(ctx.request.body).length === 0) {
      throw new Error("No data")
    }
    const data: string[] = [ctx.request.body.user_name, ctx.request.body.role, ctx.request.body.password,  ctx.request.body.email]
    const resAddUser = await addUserQuery(data)
    ctx.status = 201
    ctx.body = { success: true, data: resAddUser }
  } catch (error) {
    ctx.body = { success: false, error: error.message }
    ctx.status = 401
  }
}

export const delUserController = async (ctx: Context) => {
  try {
    const data: number[] = [ctx.params.id]
    await delUserQuery(data)
    ctx.status = 204
  } catch (error) {
    ctx.body = { success: false, error: error.message }
    ctx.status = 401
  }
}

export const updateUserController = async (ctx: Context) => {  
  try {
    if (Object.entries(ctx.request.body).length === 0) {
      throw new Error("No data")
    }    
    const data: any[] = [ctx.params.id, ctx.request.body.user_name, ctx.request.body.role, ctx.request.body.password, ctx.request.body.email]
    await updateUserQuery(data)
    ctx.status = 204
  } catch (error) {
    ctx.body = { success: false, error: error.message }
    ctx.status = 401
  }
}

export const getUsersController = async (ctx: Context) => {
  try {
    const resGetUsers = await getUsersQuery()
    ctx.status = 200
    ctx.body = { success: true, data: resGetUsers }
  } catch (error) {
    ctx.body = { success: false, error: error.message }
    ctx.status = 401
  }
}