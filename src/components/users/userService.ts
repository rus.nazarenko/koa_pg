import pool from '../../db/index'
import {createTable} from '../../db/migrations/createUsersTable'

export const addUserQuery = async (data: any[]) => {
  try {
    await createTable()
    
    const sql = `INSERT INTO users (user_name, role, password, email) VALUES ($1,$2,$3,$4)`
    await pool.query(sql, data)
    const sql2 = 'SELECT id, user_name, role, email FROM users WHERE email=$1'
    const result = await pool.query(sql2, [data[3]])
    return result.rows[0]
  } catch (err) {
    throw new Error(err)
  }
}

export const delUserQuery = async (data: any[]) => {
  try {
    const sql = 'DELETE FROM users WHERE id=$1 '
    const result = await pool.query(sql, data)
    if (result.rowCount) return
    throw new Error("No user")
  } catch (err) {
    throw new Error(err)
  }
}

export const updateUserQuery = async (data: any[]) => {
  try {
    const sql = 'UPDATE users SET user_name=$2, role=$3, password=$4, email=$5 WHERE id=$1'
    const result = await pool.query(sql, data)
    if (result.rowCount) return
    throw new Error("No user")
  } catch (err) {
    throw new Error(err)
  }
}

export const getUsersQuery = async () => {
  try {
    const sql = 'SELECT id, user_name, role, email FROM users'
    const result = await pool.query(sql)
    return result.rows
  } catch (err) {
    throw new Error(err)
  }
}