###### Application launch ######
To run the app, enter "docker-compose up --build"

###### You can use queries ######

GET http://localhost:3001/user
{
    "user_name": "Kirill",
    "password": "132",
    "role": "admin",
    "email": "kirill@ukr.net"
}

DELETE http://localhost:3001/user/2

GET http://localhost:3001/user

PUT http://localhost:3001/user/1
{
    "id": 1,
    "user_name": "Kirill",
    "role": "super-admin",
    "email": "kirill@ukr.net",
    "password": "123456"
}